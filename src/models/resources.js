import App from '../modules/app';

export class Resources {
    constructor() {
        this.list = [
            new ResourceAtlasXML('track', 'assets/spritesheets/spritesheet_tiles.png', 'assets/spritesheets/spritesheet_tiles.xml'),
            new ResourceAtlasXML('vehicles', 'assets/spritesheets/spritesheet_vehicles.png', 'assets/spritesheets/spritesheet_vehicles.xml'),
            new ResourcePhysics('track', 'assets/physics/track-tiles.json')
        ];
    }

    load() {
        return Promise.all(this.list.map((resource) => {
            return resource.load();
        }));
    }
}

class Resource {
    load() {
        throw new Error('Method "Resource.load" not implemented');
    }
}

export class ResourceAtlasXML extends Resource {
    constructor(key, image, xml) {
        super();
        Object.assign(this, {key, image, xml});
    }

    load() {
        return new Promise((resolve) => {
            App.game.load.atlasXML(this.key, this.image, this.xml).onLoadComplete.add(resolve);
        });
    }
}

export class ResourcePhysics extends Resource {
    constructor(key, json) {
        super();
        Object.assign(this, {key, json});
    }

    load() {
        return new Promise((resolve) => {
            App.game.load.physics(this.key, this.json).onLoadComplete.add(resolve);
        });
    }
}