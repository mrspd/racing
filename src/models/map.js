import App from '../modules/app';
import Tile, {TILESIZE} from '../models/tile';

const tileGroupsConfig = {
    we: [
        ['road_asphalt04', 'road_asphalt04'],
        ['road_asphalt40', 'road_asphalt40']
    ],
    ns: [
        ['road_asphalt21', 'road_asphalt23'],
        ['road_asphalt21', 'road_asphalt23']
    ],
    sw: [
        ['road_asphalt10', 'road_asphalt11'],
        ['road_asphalt28', 'road_asphalt29']
    ],
    nw: [
        ['road_asphalt46', 'road_asphalt47'],
        ['road_asphalt64', 'road_asphalt65']
    ],
    ne: [
        ['road_asphalt44', 'road_asphalt45'],
        ['road_asphalt62', 'road_asphalt63']
    ],
    se: [
        ['road_asphalt08', 'road_asphalt09'],
        ['road_asphalt26', 'road_asphalt27']
    ]
};

const tileStartGroupsConfig = {
    ns: [
        ['road_asphalt21', 'road_asphalt23'],
        ['road_asphalt69', 'road_asphalt71']
    ],
    we: [
        ['road_asphalt04', 'road_asphalt87'],
        ['road_asphalt40', 'road_asphalt89']
    ]
};

export default class Map {
    constructor(mapGenerator) {
        this.generator = mapGenerator;
        this.map = this.generator.map;
        this.path = Map.convertPathToCoordinates(this.generator.path);
        this._sprites = [];
        this._cg = App.game.physics.p2.createCollisionGroup();
    }

    get startPosition() {
        return this.generator.start;
    }

    get cg() {
        return this._cg;
    }

    get sprites() {
        return this._sprites;
    }

    static convertPathToCoordinates(path) {
        return path.map((path) => {
            return [path[0] * 2 * TILESIZE + TILESIZE, path[1] * 2 * TILESIZE + TILESIZE];
        });
    }

    draw() {
        this.map.forEach((cells, y) => {
            cells.forEach((cell, x) => {
                let isStart = false;

                if(cell) {
                    if(Map.cellIsEqual(x, y, this.startPosition.tile[1], this.startPosition.tile[0])) {
                        isStart = true;
                    }

                    this.addTileGroup(x * 2, y * 2, cell, isStart);
                }
            });
        });
    }

    addTileGroup(mx, my, name, isStart) {
        this.getTileGroupConfig(name, isStart).forEach((rows, y) => {
            rows.forEach((tileName, x) => {
                let [dx, dy] = [(x + mx) * TILESIZE + TILESIZE / 2, (y + my) * TILESIZE + TILESIZE / 2];
                let sprite = App.game.add.sprite(dx, dy, 'track', `${tileName}.png`);

                sprite.alpha = 0.5;

                App.game.physics.p2.enable(sprite, App.debug);
                sprite.body.clearShapes();
                sprite.body.loadPolygon('track', tileName);
                sprite.body.setCollisionGroup(this.cg);
                sprite.body.kinematic = true;

                this._sprites.push(sprite);
            });
        });
    }

    getTileGroupConfig(name, isStart) {
        if(isStart) {
            let group = [].concat(tileStartGroupsConfig[name]);
            if(['s', 'e'].indexOf(this.startPosition.from) == -1) {
                switch (name) {
                    case 'ns':
                        return group.reverse();
                    break;

                    case 'we':
                        return group.map((row) => row.reverse());
                        break;
                }
            } else {
                return group;
            }
        } else {
            return tileGroupsConfig[name];
        }
    }

    static cellIsEqual(x1, y1, x2, y2) {
        return x1 == x2 && y1 == y2;
    }
}