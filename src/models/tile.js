export default class Tile {
    static prepareTileMap(name, x, y, params = {}) {
        return {
            name,
            position: [x, y],
            params
        }
    }
}

export const TILESIZE = 128;
export const FINISH_LINE_WIDTH = 40;