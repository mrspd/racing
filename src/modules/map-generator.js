import randomItem from 'random-item';

const points = ['n', 's', 'w', 'e'];

const entityMap = {
    ns: "\u22EE",
    we: "\u22EF",
    nw: "\u2518",
    ne: "\u2514",
    sw: "\u2510",
    se: "\u250C",
};

const tileConnects = {
    we: ['w', 'e'],
    ns: ['n', 's'],
    nw: ['n', 'w'],
    ne: ['n', 'e'],
    sw: ['s', 'w'],
    se: ['s', 'e']
};

const mainDirectionExclude = ['w', 'n', 'e', 's'];

function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min)) + min;
}

export default class MapGenerator {
    constructor(maxSize = 15, minSegmentCount = 15, minTurnCount = 5) {
        this.maxSize = maxSize;
        this.minSegmentCount = minSegmentCount;
        this.minTurnCount = minTurnCount;
        this.width = 0;
        this.height = 0;

        this.map = new Array(this.maxSize);
        this.path = [];
        this.rotate = 0;
        this.straight = 0;
        this.mainDirection = 0;
        this.straightSegmentCells = [];

        this.segmentCount = 0;
        this.segmentTurnCount = 0;

        this.start = null;

        this.resetMap();
    }

    resetMap() {
        this.rotate = 0;
        this.mainDirection = 0;
        this.map = new Array(this.maxSize);
        this.segmentCount = 0;
        this.segmentTurnCount = 0;
        this.width = 0;
        this.height = 0;
        this.start = null;

        for(let i = 0; i < this.maxSize; i++) {
            this.map[i] = new Array(this.maxSize).fill(null);
        }
    }

    isReady() {
        if(this.tileCount < this.minSegmentCount) {
            return false;
        }

        if(this.segmentTurnCount < this.minTurnCount) {
            return false;
        }

        if(this.map[0][1] && this.map[1][0] && tileConnects[this.map[0][1]].indexOf('w') > -1 && tileConnects[this.map[1][0]].indexOf('n') > -1) {
            this.start = randomItem(this.straightSegmentCells);
            this.restructPath();
            return true;
        }

        return false;
    }

    restructPath() {
        let startCoords = this.start.tile;

        let startPoint = this.path.find((point) => {
            return point[0] == startCoords[1] && point[1] == startCoords[0];
        });

        let pathTail = this.path.splice(0, this.path.indexOf(startPoint));
        this.path = this.path.concat(pathTail);
    }

    getTile(x, y, from) {
        let exclude = [],
            nearestCells = {u: null, d: null, l: null, r: null};

        if(from && (MapGenerator.invertDirection(from) == mainDirectionExclude[this.mainDirection])) {
            exclude.push(mainDirectionExclude[this.mainDirection]);
        }

        // top
        if(y == 0) {
            exclude.push('n');
        } else {
            nearestCells.u = this.map[y-1][x];
        }

        if(y == this.maxSize - 1) {
            exclude.push('s');
        } else {
            nearestCells.d = this.map[y + 1][x];
        }

        if(x == 0) {
            exclude.push('w');
        } else {
            nearestCells.l = this.map[y][x-1];
        }

        if(x == this.maxSize - 1) {
            exclude.push('e');
        } else {
            nearestCells.r = this.map[y][x + 1];
        }

        if(nearestCells.u) {
            if(tileConnects[nearestCells.u].indexOf(MapGenerator.invertDirection('n')) === -1) {
                exclude.push('n');
            }
        }

        if(nearestCells.d) {
            if(tileConnects[nearestCells.d].indexOf(MapGenerator.invertDirection('s')) === -1) {
                exclude.push('s');
            }
        }

        if(nearestCells.l) {
            if(tileConnects[nearestCells.l].indexOf(MapGenerator.invertDirection('w')) === -1) {
                exclude.push('w');
            }
        }

        if(nearestCells.r) {
            if(tileConnects[nearestCells.r].indexOf(MapGenerator.invertDirection('e')) === -1) {
                exclude.push('e');
            }
        }

        let filteredPoints = points.filter((item) => {
            return exclude.indexOf(item) == -1;
        });

        let tiles = Object.keys(tileConnects).filter((item) => {
            let points = tileConnects[item].filter((point) => {
                return filteredPoints.indexOf(point) > -1;
            });

            if(points.length == 2) {
                if(from) {
                    if(points.indexOf(from) > -1) {
                        return true;
                    }
                } else {
                    return true;
                }
            }
        }).filter((item) => {
            return !((this.straight > 1 && ['ns', 'we'].indexOf(item) > -1) || (this.rotate > 1 && ['nw', 'ne'].indexOf(item) > -1) || (this.rotate < -1 && ['sw', 'se'].indexOf(item) > -1));
        });

        let rnd = getRandomInt(0, tiles.length);

        return tiles[rnd]
    }

    get tileCount() {
        return this.segmentCount;
    }

    static invertDirection(dir) {
        switch(dir) {
            case 'n':
                return 's';
                break;

            case 's':
                return 'n';
                break;

            case 'w':
                return 'e';
                break;

            case 'e':
                return 'w';
                break;
        }
    }

    create() {
        let dx = 0,
            dy = 0,
            attempts = 0,
            from = null;

        while(attempts < Math.pow(this.maxSize, 2)) {
            if(!this.map[dy][dx]) {
                let tile = this.getTile(dx, dy, MapGenerator.invertDirection(from));
                this.map[dy][dx] = tile;
                this.path.push([dx, dy]);

                let directions = tileConnects[tile],
                    direction = directions[directions.length - 1];

                if(from) {
                    direction = directions.filter((d) => {
                        return MapGenerator.invertDirection(d) !== from;
                    })[0];
                }

                from = direction;

                if(['we', 'ns'].indexOf(tile) > -1) {
                    this.straight++;
                    this.straightSegmentCells.push({tile: [dy, dx], from: from});
                } else {
                    this.straight = 0;
                }

                if(['nw', 'ne'].indexOf(tile) > -1) {
                    this.segmentTurnCount++;

                    if(from == 'n') {
                        this.rotate++;
                    } else {
                        this.rotate--;
                    }
                }

                if(['sw', 'se'].indexOf(tile) > -1) {
                    if(from == 's') {
                        this.rotate++;
                    } else {
                        this.rotate--;
                    }
                }

                switch(from) {
                    case 'n':
                        dy--;
                        break;

                    case 's':
                        dy++;
                        break;

                    case 'w':
                        dx--;
                        break;

                    case 'e':
                        dx++;
                        break;
                }

                if(this.mainDirection === 0 && dx == this.maxSize - 1) {
                    this.mainDirection++;
                }

                if(this.mainDirection === 1 && dy == this.maxSize - 1) {
                    this.mainDirection++;
                }

                if(this.mainDirection === 2 && dx == 0) {
                    this.mainDirection++;
                }

                this.segmentCount++;

                if(dx + 1 > this.width) this.width++;
                if(dy + 1 > this.height) this.height++;
            }

            attempts++;
        }
    }

    static generate() {
        let mapGenerator = new MapGenerator();
        while(!mapGenerator.isReady()) {
            try {
                mapGenerator = new MapGenerator();
                mapGenerator.create();
            } catch (e) {}
        }

        return mapGenerator;
    }

    static draw(map) {
        var css = document.createElement('style');
        css.innerText = '.row {clear: both; font-weight: bold} .cell {background-color: #eeeeee;border: 1px solid #fff;border-right: none;border-bottom: none;width: 30px;height: 30px;float: left;font-maxSize: 12px;text-align: center;line-height: 30px;}';
        document.head.appendChild(css);

        map.forEach((cells, y) => {
            let rowDiv = document.createElement('div');
            rowDiv.className = 'row';
            cells.forEach((cell, x) => {
                let cellDiv = document.createElement('div');
                cellDiv.className = 'cell ' + cell;
                cellDiv.innerText = entityMap[cell] || null;

                rowDiv.appendChild(cellDiv);
            });

            document.body.appendChild(rowDiv);
        });
    }
}