import 'p2';
import 'pixi';
import 'phaser';

import {Resources} from '../models/resources';
import Map from '../models/map';
import MapGenerator from '../modules/map-generator';
import Car from '../models/car';
import {TILESIZE, FINISH_LINE_WIDTH} from '../models/tile';

class App {
    prepareDOM() {
        document.body.style['marginLeft'] = '100px';
        document.body.style['marginRight'] = '100px';
        document.body.style['padding'] = 0;
        document.body.style['height'] = '100%';
        document.children[0].style['height'] = '100%';
    }

    start(width = document.body.clientWidth, height = document.body.clientHeight, debug = false) {
        this.debug = debug;
        this.game = new Phaser.Game({
            width,
            height,
            rendered: Phaser.AUTO,
            transparent: true,
            state: {
                preload: this.preload.bind(this),
                create: this.create.bind(this),
                update: this.update.bind(this),
                render: this.render.bind(this)
            }
        });

        this.loaded = false;
        this.resources = null;
        this.cursors = null;
        this.map = null;
        this.maxVelocity = 500;
        this.pathPoint = 0;
        this.currentGhostState = [];
        this.tmpGhostState = [];
        this.bestGhostState = [];
        this.velocity = 0;
        this.racingStart = false;
        this.startTime = null;
        this.bestLapTime = null;
        this.lastLapTime = null;

        this.startText = null;
        this.currentTimeText = null;
        this.bestTimeText = null;
        this.lastTimeText = null;
        this.lapCountText = null;
        this.lapCount = 0;
        this.velocity = 0;
    }



    preload() {
        this.wheelsOnTrack = 0;
        this.resources = new Resources();
        this.game.physics.startSystem(Phaser.Physics.P2JS);
        this.game.physics.p2.restitution = 1;

        this.resources.load().then(() => {
            this.map = new Map(MapGenerator.generate());
            this.loaded = true;
            this.map.draw();

            this.addControls();
            this.currentTimeText = this.game.add.text(0, 0, "LAP: 00:00:000", {fontSize: 15});
            this.currentTimeText.fixedToCamera = true;

            this.bestTimeText = this.game.add.text(0, 20, "BST: 00:00:000", {fontSize: 15});
            this.bestTimeText.fixedToCamera = true;

            this.lastTimeText = this.game.add.text(0, 40, "LST: 00:00:000", {fontSize: 15});
            this.lastTimeText.fixedToCamera = true;

            let game = this.game;

            // Группы пересечений
            let carCollisionGroup = game.physics.p2.createCollisionGroup();
            let finishCollisionGroup = game.physics.p2.createCollisionGroup();

            // Создание финишной линии
            this.finish = this.addFinishLine();
            this.finish.body.setCollisionGroup(finishCollisionGroup);
            this.finish.body.data.shapes[0].sensor = true;

            // Создание машинки
            this.car = this.addCar();
            this.car.body.setCollisionGroup(carCollisionGroup);
            this.car.body.collideWorldBounds = true;

            // Создание призрака
            this.ghostCar = this.addGhostCar();

            this.car.body.collides(this.map.cg);
            this.car.body.collides(finishCollisionGroup);
            this.finish.body.collides(carCollisionGroup);

            this.map.sprites.forEach((sprite) => {
                sprite.body.collides(carCollisionGroup);
                sprite.body.data.shapes[0].sensor = true;
            });

            this.car.body.onBeginContact.add((body, t3) => {
                if(this.finish.body.id == body.id) {
                    this.detectFinish();
                } else {
                    this.wheelsOnTrack++;
                }
            });

            this.car.body.onEndContact.add((t1, t2, t3) => {
                if(this.finish.body.id !== t1.id){
                    this.wheelsOnTrack--;
                }
            });
            this.game.world.setBounds(0, 0, 5000, 5000);
            this.game.camera.follow(this.car);

            this.startText = this.addStartText();
        });
    }

    detectFinish() {
        // Проверка что все чекпоинты пройдены
        if(!this.map.path[this.pathPoint]) {
            let lapTime = new Date() - this.startTime;

            this.lastLapTime = lapTime;
            this.updateLastTime();

            if(lapTime < this.bestLapTime || this.bestLapTime == null) {
                this.bestLapTime = lapTime;
                this.updateBestTime();
                this.bestGhostState = this.currentGhostState;
            }

            this.startTime = new Date();
            this.tmpGhostState = [].concat(this.bestGhostState);
            this.currentGhostState = [];
            this.runGhost();
            this.pathPoint = 0;
        }
    }

    create(game) {
        if (!game.device.desktop){ game.input.onDown.add(gofull, this); }
    }

    render(game) {}

    update(game) {
        if(this.loaded && this.racingStart) {
            let cursors = this.cursors,
                car = this.car,
                velocity = this.velocity;

            const FORWARD_MAX_SPEED = 650;
            const BACKWARD_MAX_SPEED = 150;

            // Если меньше 4х колес на треке, значит мы покинули трассу
            let maxVelocity = (this.wheelsOnTrack < 4) ? BACKWARD_MAX_SPEED : FORWARD_MAX_SPEED;
            console.log(velocity);
            // Ускорение назад
            if(cursors.down.isDown) {
                velocity -= 15;
            }

            // Ускорение вперед
            if(cursors.up.isDown) {
                velocity += 30;
            }

            // Замедление при отпускании кнопок
            if(!cursors.down.isDown && !cursors.up.isDown && velocity != 0) {
                velocity += velocity / Math.abs(velocity) * -10;
            }

            // Ограничение максимальной скорости
            if(Math.abs(velocity) > maxVelocity) {
                velocity = maxVelocity * (velocity / Math.abs(velocity));
            }

            // Граница между положительной и отрицательной скоростью
            if((this.velocity > 0 && velocity <= 0) || (this.velocity < 0 && velocity >= 0)) velocity = 0;

            /*Set X and Y Speed of Velocity*/
            car.body.velocity.x = velocity * Math.cos((car.angle - 90) * 0.01745);
            car.body.velocity.y = velocity * Math.sin((car.angle - 90) * 0.01745);

            let rotation = Math.max(6, 15 * (1 - Math.abs(velocity) / FORWARD_MAX_SPEED));

            /*Rotation of Car*/
            if (cursors.left.isDown)
                car.body.angularVelocity = -rotation * (velocity / 1000);
            else if (cursors.right.isDown)
                car.body.angularVelocity = rotation * (velocity / 1000);
            else
                car.body.angularVelocity = 0;

            this.velocity = velocity;
            this.checkPath();
            this.saveGhostState();
            this.runGhost();
            this.updateTime();
        }
    }

    checkPath() {
        if(this.map.path.length > 0) {
            let point = this.map.path[this.pathPoint];

            if(point) {
                let distance = Math.sqrt(Math.pow(this.car.x - point[0], 2) + Math.pow(this.car.y - point[1], 2));

                // Пометка поинтов
                if (distance < (TILESIZE + (TILESIZE * 0.25)) * 2) {
                    this.pathPoint++;
                }
            }
        }
    }

    runGhost() {
        if(this.tmpGhostState.length > 0) {
            let state = this.tmpGhostState.shift();

            this.ghostCar.alpha = 0.3;
            this.ghostCar.x = state.x;
            this.ghostCar.y = state.y;
            this.ghostCar.angle = state.angle;
        } else {
            this.ghostCar.alpha = 0;
        }
    }

    saveGhostState() {
        this.currentGhostState.push({
            x: this.car.x,
            y: this.car.y,
            angle: this.car.angle
        });
    }

    addCar() {
        let x = (TILESIZE * 2) * this.map.startPosition.tile[1] + TILESIZE,
            y = (TILESIZE * 2) * this.map.startPosition.tile[0] + TILESIZE,
            angle,
            car;

        switch (this.map.startPosition.from) {
            case 's':
                y -= FINISH_LINE_WIDTH;
                angle = 180;
                break;

            case 'n':
                y += FINISH_LINE_WIDTH;
                angle = 0;
                break;

            case 'e':
                x -= FINISH_LINE_WIDTH;
                angle = 90;
                break;

            case 'w':
                x += FINISH_LINE_WIDTH;
                angle = 270;
                break;
        }

        car = this.game.add.sprite(x, y, 'vehicles', 'car_yellow_4.png');
        car.scale.setTo(0.75, 0.75);

        let width = car.width,
            height = car.height;

        this.game.physics.p2.enable(car, this.debug);
        car.anchor.set(0.5, 0.75);
        car.body.clearShapes();
        car.body.angle = angle;

        // car wheels
        car.body.addRectangle(5, 5, -width/2 + 2.5, height / 4 - 5);
        car.body.addRectangle(5, 5, width/2 - 2.5, height / 4 - 5);
        car.body.addRectangle(5, 5, -width/2 + 2.5, -(height / 4) * 3 + 5);
        car.body.addRectangle(5, 5, width/2 - 2.5, -(height / 4) * 3 + 5);
        car.body.updateCollisionMask();

        return car;
    }

    addFinishLine() {
        const LINE_OFFSET = 15;
        let x = (TILESIZE * 2) * this.map.startPosition.tile[1] + TILESIZE,
            y = (TILESIZE * 2) * this.map.startPosition.tile[0] + TILESIZE,
            angle,
            finishLine,
            finishLineWidth = FINISH_LINE_WIDTH + LINE_OFFSET;

        switch (this.map.startPosition.from) {
            case 's':
                y += finishLineWidth;
                angle = 180;
                break;

            case 'n':
                y -= finishLineWidth;
                angle = 0;
                break;

            case 'e':
                x += finishLineWidth;
                angle = 90;
                break;

            case 'w':
                x -= finishLineWidth;
                angle = 90;
                break;
        }

        finishLine = this.game.add.sprite(x, y);
        finishLine.width = 250;
        finishLine.height = FINISH_LINE_WIDTH;

        this.game.physics.p2.enable(finishLine, this.debug);

        finishLine.body.angle = angle;

        return finishLine;
    }

    addGhostCar() {
        let ghostCar = this.game.add.sprite(0, 0, 'vehicles', 'car_red_4.png');
        ghostCar.scale.setTo(0.75, 0.75);
        ghostCar.anchor.set(0.5, 0.75);
        ghostCar.alpha = 0;

        return ghostCar;
    }

    addControls() {
        let spaceKey = this.game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
        spaceKey.onDown.add(this.startRace.bind(this));

        this.cursors = this.game.input.keyboard.createCursorKeys();
    }

    startRace() {
        let counter = 3,
            text = null;
        this.startText.destroy();

        let interval = setInterval(() => {
            if(text) {
                counter--;
                text.destroy();
            }

            if(counter == 0) {
                this.racingStart = true;
                this.startTime = new Date();

                text = this.createText('GO!!!', {
                    backgroundColor: 'white',
                    fontSize: 50
                });

                setTimeout(() => {
                    text.destroy();
                }, 300);

                clearInterval(interval);
            } else {
                text = this.createText(counter, {
                    backgroundColor: 'white',
                    fontSize: 100
                });
            }
        }, 1000);
    }

    createText(string, style) {
        let text = this.game.add.text(0, 0, string, style);

        text.x = this.game.camera.width / 2  - text.width / 2;
        text.y = this.game.camera.height / 2  - text.height / 2;
        text.fixedToCamera = true;
        text.padding.set(50, 50);

        return text;
    }

    addStartText() {
        return this.createText('Для старта нажмите "пробел"', {
            backgroundColor: 'white',
            fontSize: 30
        });
    }

    updateTime() {
        let time = new Date() - this.startTime;
        this.currentTimeText.text = 'LAP: ' + App.formatTime(time);
    }

    updateBestTime() {
        this.bestTimeText.text = 'BST: ' + App.formatTime(this.bestLapTime);
    }

    updateLastTime() {
        this.lastTimeText.text = 'LST: ' + App.formatTime(this.lastLapTime);
    }

    static formatTime(time) {
        let milliseconds = (`${time % 1000}` + "000").slice(0, 3),
            seconds = `0${Math.floor((time / 1000) % 60)}`.slice(-2),
            minutes = `0${Math.floor(time / 1000 / 60)}`.slice(-2);

        return [minutes, seconds, milliseconds].join(':');
    }
}

export default new App();