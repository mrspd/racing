import App from 'modules/app';

App.prepareDOM();
App.start();