let path = require('path');

// Plugins
let CleanWepbackPlugin = require('clean-webpack-plugin');
let HtmlWebpackPlugin = require('html-webpack-plugin');

// Phaser
let phaserModule = path.join(__dirname, 'node_modules/phaser-ce');
let phaser = path.join(phaserModule, 'build/custom/phaser-split.js');
let pixi = path.join(phaserModule, 'build/custom/pixi.js');
let p2 = path.join(phaserModule, 'build/custom/p2.js');

module.exports = {
    entry: './src/index.js',
    output: {
        filename: 'bundle.js',
        path: path.resolve(__dirname, 'build')
    },
    resolve: {
        modules: ['src', 'node_modules'],
        alias: {
            'phaser': phaser,
            'pixi': pixi,
            'p2': p2,
        },
    },
    plugins: [
        new CleanWepbackPlugin(['build']),
        new HtmlWebpackPlugin(),
    ],
    module: {
        rules: [
            {test: /\.js$/, use: ['babel-loader'], include: path.join(__dirname, 'src')},
            {test: /pixi\.js/, use: ['expose-loader?PIXI']},
            {test: /phaser-split\.js$/, use: ['expose-loader?Phaser']},
            {test: /p2\.js/, use: ['expose-loader?p2']}
        ],
    },
    devServer: {
        contentBase: [path.join(__dirname, "build"), path.join(__dirname, "src")]
    }
};